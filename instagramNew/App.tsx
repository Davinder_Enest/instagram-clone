import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {RootNavigation} from './src/routes';
import colors from './src/consts/colors';
import { StatusBar } from 'react-native';

const App = () => {
  return (
    <NavigationContainer>
      <StatusBar backgroundColor={colors.WHITE} barStyle="dark-content" />
      <RootNavigation />
    </NavigationContainer>
  );
}

export default App;