import { StyleSheet } from 'react-native';
import colors from '../../../consts/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.WHITE
    },
    container2: {
        backgroundColor: colors.WHITE,
        flexDirection: 'row',
        justifyContent: "space-between",
        height: 40,
        borderBottomWidth: 1,
        borderBottomColor: colors.GREY,
    },
    logo: {
        width: 135,
        height: 40
    },
    camera: {
        marginLeft: 10,
        marginTop: 7
    },
    messages: {
        marginRight: 10,
        marginTop: 7
    }
});

export default styles;