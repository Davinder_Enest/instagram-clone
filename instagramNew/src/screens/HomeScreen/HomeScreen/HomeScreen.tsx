import React, { Component } from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import Posts from '../../../components/HomeScreen/Posts/Posts';
import Story from '../../../components/HomeScreen/Story/Story';
import Feather from "react-native-vector-icons/Feather";
import Ionicons from "react-native-vector-icons/Ionicons";
import styles from './HomeScreen.styles';
import { NavigationParams, NavigationScreenProp, NavigationState, } from 'react-navigation';

interface Props {
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

class Home extends Component<Props>{
    constructor(Props: any){
        super(Props);
    }
    onPressMessages=()=>{
        this.props.navigation.navigate('Messages');
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.container2}>
                    <Feather
                        style={styles.camera}
                        name="camera" size={25} color={'#000'}
                    />
                    <Image source={require('../../../assets/images/logo.png')} style={styles.logo} resizeMode="contain" />
                    <TouchableOpacity
                        onPress={()=>this.onPressMessages()}>
                        <Ionicons
                            style={styles.messages}
                            name="paper-plane-outline" size={25} color={"#000"} />
                    </TouchableOpacity>
                </View>
                    <Story />
                    <Posts />
            </View>
        );
    }
}

export default Home;