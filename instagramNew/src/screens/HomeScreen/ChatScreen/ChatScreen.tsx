import { GiftedChat, IMessage, Bubble } from 'react-native-gifted-chat';
import React from 'react';
import { View, StatusBar } from 'react-native';
import styles from './ChatScreen.styles';
import colors from '../../../consts/colors'
interface State {
  messages: IMessage[];
}

class ExampleChat extends React.Component<State, any>{
  constructor(props: any) {
    super(props);
    this.state = { messages: [] };
    this.onSend = this.onSend.bind(this);
  }
  componentDidMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Hello developer',
          createdAt: new Date(Date.UTC(2016, 7, 30, 17, 20, 0)),
          avatar: require('../../../assets/images/First.jpg'),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://facebook.github.io/react/img/logo_og.png',
          },
        },
      ],
    });
  }
  onSend(messages = []) {
    this.setState((previousState: { messages: never[] | undefined; }) => {
      return {
        messages: GiftedChat.append(previousState.messages, messages),
      };
    });
  }
  renderBubble(props: any) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: {
            backgroundColor: colors.GREY,
          },
          right: {
            backgroundColor: colors.SKYBLUE
          }
        }} 
      />
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle='light-content'  ></StatusBar>
        <GiftedChat
          messages={this.state.messages}
          onSend={this.onSend}
          renderBubble={this.renderBubble}
          user={{
            _id: 1,
          }}
        />
      </View>
    );
  }
}

export default ExampleChat;