import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../../consts/colors';

const styles = StyleSheet.create({
    keyBoardAvoiding :{
        flex:1
    },
    container: {
        flex: 1,
        backgroundColor: colors.WHITE,
        width: Dimensions.get('screen').width,
    },
    searchBox:{
        backgroundColor: colors.TEXTINPUT,
        height: 40,
        color : colors.PLACEHOLDER,
        margin : 10,
        borderRadius : 10
    },
    requestsMessage: {
        height: 25,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
        margin: 7,
        marginLeft:13
    },
    messageText: {
        fontWeight: "bold",
    },
    requestText: {
        color: colors.SKYBLUE
    },
    chatList: {
        flexDirection: 'row',
    },
    profileImage: {
        height: 50,
        width: 50,
        borderRadius: 50,
        margin: 8
    },
    MiddleView: {
        justifyContent: 'center',
        width: '70%'
    },
    LeftView: {
        justifyContent: 'center',
    },
    chatInfo: {
        color: colors.LIGHTGREY
    },
    chatPersonName: {
        color: colors.DARKGREY,
    },
    footer: {
        flexDirection: 'row',
        height: 40,
        alignItems: 'center',
        borderTopWidth: 1,
        borderTopColor: colors.GREY,
        width:'100%',
        justifyContent:'center'
    },
    footerText: {
        color: colors.SKYBLUE,
        marginLeft: 3,
        marginTop: 2
    }
});

export default styles;