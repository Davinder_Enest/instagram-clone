import React, { Component } from 'react';
import { Text, View, Image,TouchableOpacity, ScrollView, TextInput, KeyboardAvoidingView,TouchableWithoutFeedback,Platform, Keyboard } from 'react-native';
import Feather from "react-native-vector-icons/Feather";
import Entypo from "react-native-vector-icons/Entypo";
import styles from './ChatListScreen.styles';
import colors from '../../../consts/colors';
import { NavigationParams, NavigationScreenProp, NavigationState, } from 'react-navigation';

interface Props {
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

class User extends Component<Props> {
    constructor(props: any) {
        super(props);

    }
    onClicking() {
        console.log("click"),
            this.props.navigation.navigate('Chat')
    }
    render() {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS == "ios" ? "padding" : "height"} style={styles.keyBoardAvoiding}
            >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.container}>
                    <ScrollView>
                        <TextInput placeholder="Search" style={styles.searchBox}></TextInput>
                        <View style={styles.requestsMessage}>
                            <Text style={styles.messageText}>Messages</Text>
                            <Text style={styles.requestText}>3 requests</Text>
                        </View>
                        <View style={styles.chatList}>
                            <Image
                                style={styles.profileImage}
                                source={require('../../../assets/images/First.jpg')}
                            />
                            <View style={styles.MiddleView}>
                                <TouchableOpacity onPress={this.onClicking.bind(this)}>
                                    <Text style={styles.chatPersonName}>Shadma</Text>
                                </TouchableOpacity>
                                <Text style={styles.chatInfo}>Last seen yesterday</Text>
                            </View>
                            <View style={styles.LeftView}>
                                <Feather
                                    name="camera" size={25} color={colors.LIGHTGREY} />
                            </View>
                        </View>
                        <View style={styles.chatList}>
                            <Image
                                style={styles.profileImage}
                                source={require('../../../assets/images/First.jpg')}
                            />
                            <View style={styles.MiddleView}>
                                <Text style={styles.chatPersonName}>Sumeet</Text>
                                <Text style={styles.chatInfo}>Okay</Text>
                            </View>
                            <View style={styles.LeftView}>
                                <Feather
                                    name="camera" size={25} color={colors.LIGHTGREY} />
                            </View>
                        </View>
                        <View style={styles.chatList}>
                            <Image
                                style={styles.profileImage}
                                source={require('../../../assets/images/First.jpg')}
                            />
                            <View style={styles.MiddleView}>
                                <Text style={styles.chatPersonName}>Anchal</Text>
                                <Text style={styles.chatInfo}>Active now</Text>
                            </View>
                            <View style={styles.LeftView}>
                                <Feather
                                    name="camera" size={25} color={colors.LIGHTGREY} />
                            </View>
                        </View>
                        <View style={styles.chatList}>
                            <Image
                                style={styles.profileImage}
                                source={require('../../../assets/images/First.jpg')}
                            />
                            <View style={styles.MiddleView}>
                                <Text style={styles.chatPersonName}>Davi</Text>
                                <Text style={styles.chatInfo}>Sent an IGTV video</Text>
                            </View>
                            <View style={styles.LeftView}>
                                <Feather
                                    name="camera" size={25} color={colors.LIGHTGREY} />
                            </View>
                        </View>
                        <View style={styles.chatList}>
                            <Image
                                style={styles.profileImage}
                                source={require('../../../assets/images/First.jpg')}
                            />
                            <View style={styles.MiddleView}>
                                <Text style={styles.chatPersonName}>Shubham</Text>
                                <Text style={styles.chatInfo}>Hii</Text>
                            </View>
                            <View style={styles.LeftView}>
                                <Feather
                                    name="camera" size={25} color={colors.LIGHTGREY} />
                            </View>
                        </View>
                    </ScrollView>
                    <View style={styles.footer}>
                        <Entypo name="camera" size={25} color={colors.SKYBLUE} />
                        <Text style={styles.footerText}>Camera</Text>
                    </View>
                </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>    
        );
    }
}

export default User;