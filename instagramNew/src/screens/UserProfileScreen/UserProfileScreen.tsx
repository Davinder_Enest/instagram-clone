import React, { Component } from 'react';
import { Text, View, Button } from 'react-native';
import { firebase } from '@react-native-firebase/auth';
import { RouteProp,} from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { AllRoutesName } from '../../routes';

interface Props {
    navigation: StackNavigationProp<AllRoutesName, 'User'>;
    route : RouteProp<AllRoutesName, 'User'>;
}
  
class User extends Component<Props> {
    signOut = async() => {
        try{
            await firebase.auth().signOut()
        }
        catch(e){
            console.log(e)
        }
    }

    render() {
        return (
            <View>
                <Button title="logout" onPress={()=>{ this.signOut()}}/>
                <Text>Hello, Search here!</Text>
            </View>
        );
    }
}

export default User;