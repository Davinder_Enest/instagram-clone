import { StyleSheet } from 'react-native';
import colors from '../../consts/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.WHITE,
        justifyContent:'center',
    },
    text:{
        fontSize:20,
        fontFamily:"bold",
        alignSelf:'center'
    },
    image:{
        height :'100%',
        width: '100%',
        flex: 1,
        resizeMode: "cover",
    }
})

export default styles;