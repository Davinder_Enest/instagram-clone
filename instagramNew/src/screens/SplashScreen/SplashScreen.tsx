import React, { Component ,useEffect} from 'react';
import { Text, View, Image, ImageBackground } from 'react-native';
import styles from '../SplashScreen/SplashScreen.styles';
import { firebase } from '@react-native-firebase/auth';
import { useNavigation } from '@react-navigation/native';

function SplashScreen () {
    const navigation = useNavigation();
    useEffect(()=>{
        NavigationToAuth()
    },[navigation])

    function NavigationToAuth(){
        setTimeout(function(){
            firebase.auth().onAuthStateChanged((user) => {
                if (user != null){
                    navigation.reset({
                        index:0,
                        routes : [{name : "SignUpStackScreen"}]
                    })
                }
                else{
                    navigation.reset({
                        index:0,
                        routes : [{name : "SignUp"}]
                    })
                }
            })
            
        },1000)
    }

    return (
        <View style={styles.container}>
            <ImageBackground source={require('../../assets/images/SplashScreen.jpg')} style={styles.image}/>
        </View>
    );
}

export default SplashScreen;