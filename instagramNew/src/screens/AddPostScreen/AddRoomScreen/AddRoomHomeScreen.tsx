import React, { Component, useEffect } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import Loading from '../../../components/Loading/Loading';
import Ionicons from "react-native-vector-icons/Ionicons";
import styles from './AddRoomHomeScreen.styles';
import colors from '../../../consts/colors';
import { List, Divider } from 'react-native-paper';
import { NavigationParams, NavigationScreenProp, NavigationState, } from 'react-navigation';

interface Props {
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}
interface latest{
    text: string
}
interface Threads {
    latestMessage: latest;
    _id: string,
    name: string,
}
interface States {
    threads: Threads[],
    loading: boolean,
    // thread : any|undefined
}

class AddRoomHomeScreen extends Component<Props, States>  {
    constructor(props: any) {
        super(props);
        this.state = {
            threads: [],
            loading: true,
            // thread :''
        }
    }
    componentDidMount() {
        const unsubscribe = firestore()
            .collection('THREADS')
            .orderBy('latestMessage.createdAt', 'desc')
            .onSnapshot(querySnapshot => {
                const threads = querySnapshot.docs.map(documentSnapshot => {
                    return {
                        _id: documentSnapshot.id,
                        name: '',
                        latestMessage: {
                            text: ''
                        },
                        ...documentSnapshot.data()
                    };
                });

                this.setState({ threads: threads });
                if (this.state.loading) {
                    this.setState({ loading: false });
                }
            });
        return () => unsubscribe();
    }
    AddRoom = () => {
        this.props.navigation.navigate('AddRoom');
    }
    onPressBack = () => {
        this.props.navigation.goBack();
    }
    render() {
        if (this.state.loading) {
            return <Loading />;
        }
        return (
            <View style={styles.container}>
                <View style={styles.container2}>
                    <TouchableOpacity
                        onPress={() => this.onPressBack()}>
                        <Ionicons name="arrow-back" size={25} color={colors.BLACK} />
                    </TouchableOpacity>
                    <Text style={styles.headerTitle}> All Rooms </Text>
                    <TouchableOpacity
                        onPress={() => this.AddRoom()}>
                        <Ionicons name="add-circle-outline" size={25} color={colors.BLACK} />
                    </TouchableOpacity>
                </View>

                <FlatList
                    data={this.state.threads}
                    keyExtractor={item => item._id}
                    renderItem={({ item }) => (
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('RoomScreen', { id: item._id })}
                        >
                            <List.Item
                                title={item.name}
                                description={item.latestMessage.text}
                                titleNumberOfLines={1}
                                titleStyle={styles.listTitle}
                                descriptionStyle={styles.listDescription}
                                descriptionNumberOfLines={1}
                            />
                        </TouchableOpacity>
                    )}
                />
            </View>
        );

    }
}

export default AddRoomHomeScreen;