import React from 'react';
import {  StyleSheet } from 'react-native';
import colors from '../../../consts/colors';

const styles = StyleSheet.create({
    container: {
      backgroundColor: colors.WHITE,
      flex: 1,
    },
    container2: {
      backgroundColor: colors.GREY,
      flexDirection: 'row',
      justifyContent: "space-between",
      height: 50,
      alignItems:'center',
  },
  headerTitle: {
    fontSize: 18,
    marginLeft:10
  },
  listDescription: {
    fontSize: 14,
    marginLeft:15
  },
  flatlist:{
    height:40,
    justifyContent:"center",
    borderBottomWidth :1,
    marginTop: 5,
    borderBottomColor :colors.GREY,
    
  },
  listTitle: {
    fontSize: 18,
    marginLeft:15
  },
  });

export default styles;