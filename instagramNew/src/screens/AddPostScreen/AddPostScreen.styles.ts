import { StyleSheet } from 'react-native';
import colors from '../../consts/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor :colors.WHITE,
        justifyContent:'center',
    },
    button:{
        width:'50%'
    }
})

export default styles;