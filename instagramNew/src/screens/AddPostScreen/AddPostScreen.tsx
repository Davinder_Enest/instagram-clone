import React, { Component } from 'react';
import { View, Button, Text, FlatList, TouchableOpacity } from 'react-native';
import styles from './AddPostScreen.styles';
import { NavigationParams, NavigationScreenProp, NavigationState, } from 'react-navigation';

interface Props {
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}
interface States{  
}

class AddPost extends Component<Props,States>  {
  onClicking() {
    this.props.navigation.navigate('AddRoomHomeScreen')
  }
  render(){
    return (
        <View style={styles.container}>
          <Button 
          onPress={this.onClicking.bind(this)} title="Go to Chat Room"></Button>
        </View>
      );
    
  }
}

export default AddPost;