import { StyleSheet } from 'react-native';
import colors from '../../../consts/colors';
const styles = StyleSheet.create({
    container:{
      flex:1
    },
    loadingContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    sendingContainer: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    bottomComponentContainer: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    systemMessageWrapper: {
      backgroundColor: '#6646ee',
      borderRadius: 4,
      padding: 5
    },
    systemMessageText: {
      fontSize: 14,
      color: '#fff',
      fontWeight: 'bold'
    }
  });

export default styles;