import { GiftedChat, IMessage, Bubble, SystemMessage } from 'react-native-gifted-chat';
import React from 'react';
import { View, ActivityIndicator} from 'react-native';
import styles from './RoomScreen.styles';
import colors from '../../../consts/colors';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

interface State {
  messages: IMessage[];
  roomId: string

}
interface Props {
  route: any
}

function renderLoading() {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size='large' color={colors.SKYBLUE} />
      </View>
    );
  }

class RoomScreen extends React.Component<Props, State>{
  
  constructor(props: Props) {
    super(props);
    this.state = { 
      messages: [], 
      roomId: this.props.route.params.id
    };
    this.onSend = this.onSend.bind(this);
  }
  
  componentDidMount() {
    const user = auth().currentUser;
      const messagesListener = firestore()
        .collection('THREADS')
        .doc(this.state.roomId)
        .collection('MESSAGES')
        .orderBy('createdAt', 'desc')
        .onSnapshot((querySnapshot: { docs: any[]; }) => {
          const messages: any = querySnapshot.docs.map(doc => {
            const firebaseData = doc.data();
            const data = {
              _id: doc.id,
              text: '',
              createdAt: new Date().getTime(),
              ...firebaseData
            };
            if (!firebaseData.system) {
              data.user = {
                ...firebaseData.user,
                name: firebaseData.user.email
              };
            }
            return data;
          });
          this.setState(messages);
        });
      return () => messagesListener();
  }
  async onSend(messages: IMessage[]) {
    const text = messages[0].text;
    const user = auth().currentUser;
    firestore()
      .collection('THREADS')
      .doc(this.state.roomId)
      .collection('MESSAGES')
      .add({
        text,
        createdAt: new Date().getTime(),
        user: {
          _id: user?.uid,
        }
      });

    await firestore()
      .collection('THREADS')
      .doc(this.state.roomId)
      .set(
        {
          latestMessage: {
            text,
            createdAt: new Date().getTime()
          }
        },
        { merge: true }
      );
  }
  renderBubble(props: any) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: {
            backgroundColor: colors.GREY,
          },
          right: {
            backgroundColor: colors.SKYBLUE
          }
        }} 
      />
    )
  }
  renderSystemMessage(props:any) {
    return (
      <SystemMessage
        {...props}
        wrapperStyle={styles.systemMessageWrapper}
        textStyle={styles.systemMessageText}
      />
    );
  }
  render() {
    return (
      <View style={styles.container}>
        <GiftedChat
          messages={this.state.messages}
          onSend={this.onSend}
          renderBubble={this.renderBubble}
          user={{
            _id: 1, name : auth().currentUser?.uid
          }}
          placeholder='Type your message here...'
          showUserAvatar
          scrollToBottom
          alwaysShowSend
          renderLoading={renderLoading}
          renderSystemMessage={this.renderSystemMessage}
        />
      </View>
    );
  }
}

export default RoomScreen;
