import React, { useState, Component } from 'react';
import { View, StyleSheet, Text, TextInput, Button, Alert, TouchableOpacity } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import Ionicons from "react-native-vector-icons/Ionicons";
import colors from '../../../consts/colors';
import { NavigationParams, NavigationScreenProp, NavigationState, } from 'react-navigation';

interface Props {
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}
interface State{
    roomName: string,
}

export default class AddRoom extends Component<Props,State> {
    constructor(props:any){
        super(props);
        this.state={
            roomName : ''
        }
    }
    handleButtonPress=()=>{
        try{
        if (this.state.roomName.length > 0) {
            firestore()
              .collection('THREADS')
              .add({
                name: this.state.roomName,
                latestMessage: {
                  text: `You have joined the room ${this.state.roomName}.`,
                  createdAt: new Date().getTime()
                }
                }
              )
              .then(docRef => {
                docRef.collection('MESSAGES').add({
                  text: `You have joined the room ${this.state.roomName}.`,
                  createdAt: new Date().getTime(),
                  system: true
                });
                this.props.navigation.navigate('AddRoomHomeScreen');
              });
          }}
          catch(error){
            Alert.alert(error.message)
            console.log(error)
        }
    }
    onPressClose = () => {
      this.props.navigation.goBack();
    }
    render(){
        const setRoomName=this.state.roomName
    return (
      <View style={styles.rootContainer}>
        <View style={styles.closeButtonContainer}>
          <TouchableOpacity
            onPress={() => this.onPressClose()}>
            <Ionicons name="close-circle-outline" size={25} color={colors.BLACK} />
          </TouchableOpacity>
        </View>
        <View style={styles.innerContainer}>
          <Text style={styles.title}>Create your own chat room</Text>
          <TextInput
            style={styles.input}
            placeholder='Room Name'
            value={this.state.roomName}
            onChangeText={(text) => this.setState({roomName:text})}
          />
          <Button
            title='Create'
            onPress={() => this.handleButtonPress()}
            disabled={this.state.roomName.length === 0}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    rootContainer: {
      flex: 1,
    },
    closeButtonContainer: {
      position: 'absolute',
      top: 10,
      right: 10,
      zIndex: 1,
    },
    innerContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    title: {
      fontSize: 20,
      marginBottom: 10,
      marginLeft:6
    },
    buttonLabel: {
      fontSize: 22,
      width:'50%'
    },
    input: {
      width: '60%',
      borderColor: colors.GREY,
      backgroundColor: colors.WHITE,
      borderWidth: 1,
      margin: 5,
      height: 40,
      borderRadius: 3,
      color: '#707070',
      marginLeft: 20,
      marginRight: 10
  },
  });