import React, { Component } from 'react';
import { Text, View, ImageBackground, Image, Alert, Button, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import styles from './SignUpScreen.styles';
import auth from '@react-native-firebase/auth';
import { NavigationParams, NavigationScreenProp, NavigationState, } from 'react-navigation';

interface Props {
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}
interface States {
    inputUsername: string,
    phone: string,
    confirmResult: null,
    verificationCode: string,
    userId: string
}

class AddPost extends Component<Props, States> {
    constructor(props: any) {
        super(props);
        this.state = {
            inputUsername: '',
            phone: '',
            confirmResult: null,
            verificationCode: '',
            userId: ''
        }
    }
    validatePhoneNumber = () => {
        var regexp = /^\+[0-9]?()[0-9](\s|\S)(\d[0-9]{8,16})$/
        return regexp.test(this.state.phone)
    }

    handleSendCode = () => {
        if (this.validatePhoneNumber()) {
            auth().signInWithPhoneNumber(this.state.phone)
                .then(confirmResult => {
                    this.setState({ confirmResult })
                })
                .catch(error => {
                    Alert.alert(error.message)
                    console.log(error)
                })
        } else {
            Alert.alert('Invalid Phone Number')
        }
    }

    changePhoneNumber = () => {
        this.setState({ confirmResult: null, verificationCode: '' })
    }

    handleVerifyCode = () => {
        const { confirmResult, verificationCode } = this.state
        if (verificationCode.length == 6) {
            confirmResult
                .confirm(verificationCode)
                .then((user: any) => {
                    this.setState({ userId: user.uid })
                    this.props.navigation.navigate('SignUpStackScreen');
                })
                .catch((error: { message: string; }) => {
                    Alert.alert(error.message)
                    console.log(error)
                })
        } else {
            Alert.alert('Please enter a 6 digit OTP code.')
        }
    }

    renderConfirmationCodeView = () => {
        return (
            <View style={styles.verificationView}>
                <TextInput
                    style={styles.input}
                    placeholder='Verification code'
                    value={this.state.verificationCode}
                    keyboardType='numeric'
                    onChangeText={verificationCode => {
                        this.setState({ verificationCode })
                    }}
                    maxLength={6}
                />
                <TouchableOpacity
                    style={styles.signupButton}
                    onPress={this.handleVerifyCode}>
                    <Text style={styles.ButtonTitle}>Verify Code</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <Image
                    source={require('../../assets/images/logo.png')}
                    style={styles.logo}
                />
                <Text
                    style={styles.headerText}>
                    Signup to see photos and videos from your friends.
                </Text>
                <TextInput
                    style={styles.input}
                    placeholder='Phone Number with country code'
                    keyboardType='phone-pad'
                    value={this.state.phone}
                    onChangeText={phone => {
                        this.setState({ phone })
                    }}
                    maxLength={15}
                    editable={this.state.confirmResult ? false : true}
                />

                <TouchableOpacity
                    style={styles.signupButton}
                    onPress={
                        this.state.confirmResult
                            ? this.changePhoneNumber
                            : this.handleSendCode
                    }>
                    <Text style={styles.ButtonTitle}>
                        {this.state.confirmResult ? 'Change Phone Number' : 'Send Code'}
                    </Text>
                </TouchableOpacity>

                {this.state.confirmResult ? this.renderConfirmationCodeView() : null}
                
                <TouchableOpacity
                    style={styles.footer}>
                    <Text style={styles.footerText}>Already have an account?</Text>
                    <Text style={styles.loginText}>Login</Text>
                </TouchableOpacity>
                <Text
                    style={styles.agreeText}>
                    By signing up, you agree to our Terms , Data Policy and Cookies Policy.
                </Text>

            </View>
        );
    }
}

export default AddPost;