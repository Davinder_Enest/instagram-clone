import { StyleSheet } from 'react-native';
import colors from '../../consts/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor :colors.WHITE,
        justifyContent:'center',
    },
    verificationView: {
        width: '100%',
        alignItems: 'center',
    },
    logo: {
        width: 155,
        height: 50,
        margin: 10,
        marginLeft: 105,
        marginRight: 50
    },
    headerText: {
        fontSize: 18,
        color: '#707070',
        marginBottom: 5,
        fontWeight: '200',
        marginLeft: 20,
        marginRight: 10,
        textAlign: 'center',
    },
    input: {
        width: '90%',
        borderColor: colors.GREY,
        backgroundColor: '#FAFAFA',
        borderWidth: 1,
        margin: 5,
        height: 40,
        borderRadius: 3,
        color: '#707070',
        marginLeft: 20,
        marginRight: 10
    },
    signupButton: {
        width: '90%',
        backgroundColor: colors.SKYBLUE,
        margin: 5,
        height: 35,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 10
    },
    signupText: {
        color: colors.WHITE,
        fontSize: 16,
    },
    footer: {
        width: '90%',
        backgroundColor: '#FAFAFA',
        margin: 5,
        height: 35,
        borderRadius: 3,
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 10,
        justifyContent: 'center',
        alignContent: 'center',
        flexDirection: 'row',

    },
    footerText: {
        color: '#707070'
    },
    loginText: {
        color: colors.SKYBLUE,
        fontWeight: 'bold',
        justifyContent: 'center'
    },
    agreeText: {
        textAlign: 'center',
        marginTop: 30,
        color: '#707070',
    },
    ButtonTitle:{
        color: colors.WHITE,
        fontSize: 16,
    }
})

export default styles;