const colors = {
    STATUSBAR : 'light-content',
    WHITE: '#FFFFFF',
    GREY :"#C8C8C8",
    TEXTINPUT : "#F5F5F5",
    PLACEHOLDER :'#989898',
    SKYBLUE : '#007FFF',
    LIGHTGREY :'#989898',
    DARKGREY : '#383838',
    CHATBACKGROUND : '#888888',
    BLACK : '#000'
}
export default colors ;