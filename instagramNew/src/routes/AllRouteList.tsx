export type AllRoutesName = {
    Home : {},
    Messages : {},
    Chat :{},
    Search : {},
    AddPost : {},
    Notifications : {},
    User : {},
    SignUp :{},
    AddRoomHomeScreen:{},
    AddRoom:{},
    RoomScreen:{},
    SignUpStack:{},
    SignUpStackScreen:{},
    SplashScreen :{}
}