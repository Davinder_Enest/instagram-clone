import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Feather from "react-native-vector-icons/Feather";

import Home from '../screens/HomeScreen/HomeScreen/HomeScreen';
import MessageScreen from '../screens/HomeScreen/ChatListScreen/ChatListScreen';
import ExampleChat from '../screens/HomeScreen/ChatScreen/ChatScreen';
import SignUpScreen from '../screens/SignUpScreen/SignUpScreen';
import { AllRoutesName } from './AllRouteList';
import colors from '../consts/colors';

const HomeStack = createStackNavigator<AllRoutesName>();

export function HomeStackScreen() {
    return (
        <HomeStack.Navigator
            initialRouteName="SignUp"
        >
            <HomeStack.Screen name="Home" component={Home}
                options={{
                    headerShown: false
                }} />
            <HomeStack.Screen name="Messages" component={MessageScreen}
                options={{
                    title: 'preetyBhutta',
                    headerShown: true,
                    headerTitleAlign: 'center',
                    headerLeftContainerStyle: {
                        marginLeft: -5,
                    },
                    headerRightContainerStyle: {
                        marginRight: 15,
                    },
                    headerRight: () => (
                        <Feather name="video" size={25} color={colors.DARKGREY} />
                    )
                }}
            />
            <HomeStack.Screen name="Chat" component={ExampleChat} />
        </HomeStack.Navigator>
    );
}


