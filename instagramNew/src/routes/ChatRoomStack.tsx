import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import AddPost from '../screens/AddPostScreen/AddPostScreen';
import AddRoomScreen from '../screens/AddPostScreen/CreateRoomScreen/AddRoomScreen';
import { AllRoutesName } from './AllRouteList';
import RoomScreen from '../screens/AddPostScreen/RoomScreen/RoomScreen';
import AddRoomHomeScreen from '../screens/AddPostScreen/AddRoomScreen/AddRoomHomeScreen';

const ChatAppStack = createStackNavigator<AllRoutesName>();

export function ChatApp() {
    return (
        <ChatAppStack.Navigator
            initialRouteName="AddPost"
        >
            <ChatAppStack.Screen name='AddPost' component={AddPost}
                options={{
                    headerShown: false,
                }} />
            <ChatAppStack.Screen name='AddRoomHomeScreen' component={AddRoomHomeScreen}
                options={{
                    headerShown: false,
                }} />
            <ChatAppStack.Screen name='AddRoom' component={AddRoomScreen}
                options={{
                    headerShown: false,
                }} />
            <ChatAppStack.Screen
                //options={({ route }) => ({ title: route.params.thread.item.name })}
                name='RoomScreen' component={RoomScreen} />
        </ChatAppStack.Navigator>
    );
}

