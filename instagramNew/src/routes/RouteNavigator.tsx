import * as React from 'react';
import { createBottomTabNavigator, BottomTabBar } from '@react-navigation/bottom-tabs';

import Foundation from "react-native-vector-icons/Foundation";
import Ionicons from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";
import AntDesign from "react-native-vector-icons/AntDesign";

import Search from '../screens/SearchScreen/SearchScreen';
import Notifications from '../screens/NotificationsScreen/NotificationsScreen';
import User from '../screens/UserProfileScreen/UserProfileScreen';
import { AllRoutesName } from './AllRouteList';
import { HomeStackScreen } from './HomeStack';
import { ChatApp } from './ChatRoomStack';
import colors from '../consts/colors';
import { createStackNavigator } from '@react-navigation/stack';
import { SignUpStackScreen } from './SignUpStack';
import SignUpScreen from '../screens/SignUpScreen/SignUpScreen';
import SplashScreen from '../screens/SplashScreen/SplashScreen';

const SignUpStack = createStackNavigator<AllRoutesName>();

export function RootNavigation() {
  return (
    <SignUpStack.Navigator
        initialRouteName="SplashScreen"
    >
        <SignUpStack.Screen name="SplashScreen" component={SplashScreen}
            options={{
                headerShown: false,
            }}
        />
        <SignUpStack.Screen name="SignUp" component={SignUpScreen}
            options={{
                headerShown: false
            }}
        />
        <SignUpStack.Screen name="SignUpStackScreen" component={SignUpStackScreen}
            options={{
                headerShown: false,
            }}
        />
    </SignUpStack.Navigator>
  );
}

