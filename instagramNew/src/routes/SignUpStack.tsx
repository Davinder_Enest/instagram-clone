import * as React from 'react';
import { createBottomTabNavigator, BottomTabBar } from '@react-navigation/bottom-tabs';

import Foundation from "react-native-vector-icons/Foundation";
import Ionicons from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";
import AntDesign from "react-native-vector-icons/AntDesign";

import Search from '../screens/SearchScreen/SearchScreen';
import Notifications from '../screens/NotificationsScreen/NotificationsScreen';
import User from '../screens/UserProfileScreen/UserProfileScreen';
import { AllRoutesName } from './AllRouteList';
import { HomeStackScreen } from './HomeStack';
import { ChatApp } from './ChatRoomStack';
import colors from '../consts/colors';

const Tab = createBottomTabNavigator<AllRoutesName>();

function getTabBarVisible(route: any) {
  const routeName = route.state
    ? route.state.routes[route.state.index].name
    : route.params?.screen || 'Home';

  if ( routeName === 'Messages' || routeName === 'Chat'
    || routeName === 'AddRoom' || routeName === 'AddRoomHomeScreen' || routeName === 'RoomScreen') {
    return false;
  }
  return true;
}

export function SignUpStackScreen() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: colors.BLACK,
        inactiveTintColor: 'gray',
        showLabel: false,
      }}
    >
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        options={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => (
            <Foundation name="home" color={color} size={size} />
          ),
          tabBarVisible: getTabBarVisible(route)
        })}
      />
      <Tab.Screen
        name="Search"
        component={Search}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Feather name="search" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="AddPost"
        component={ChatApp}
        options={({ route }) => ({
          tabBarIcon: ({ color, size }) => (
            <Feather name="plus-square" color={color} size={size} />
          ),
          tabBarVisible: getTabBarVisible(route)
        })}

      />
      <Tab.Screen
        name="Notifications"
        component={Notifications}
        options={{
          tabBarIcon: ({ color, size }) => (
            <AntDesign name="hearto" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="User"
        component={User}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="person-outline" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}




