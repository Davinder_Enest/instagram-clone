import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';

export default function Loading() {
  return (
    <View style={styles.loadingContainer}>
      <ActivityIndicator size='large' color='#6646ee' />
    </View>
  );
}

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
/*import React, { useState, Component ,useEffect} from 'react';
import { View, StyleSheet, Button, Text, FlatList, TouchableOpacity } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import Loading from '../../components/Loading/Loading';

interface Threads {
    _id: string;
    name: string;
    latestMessage: {
        text: string;
    };
}

interface Props{
    navigation : any
}
interface States{
    threads: Threads[],
    loading : boolean
}

class AddPost extends Component<Props,States>  {
    constructor(props:any){
        super(props);
        this.state={
            threads : [],
            loading: true,
        }
    }

    onClicking() {
            this.props.navigation.navigate('AddRoom')
    }
    componentDidMount() {
        const unsubscribe = firestore()
          .collection('THREADS')
          .orderBy('latestMessage.createdAt', 'desc')
          .onSnapshot(querySnapshot => {
            const threads = querySnapshot.docs.map(documentSnapshot => {
              return {
                _id: documentSnapshot.id,
                // give defaults
                name: '',
    
                latestMessage: {
                  text: ''
                },
                ...documentSnapshot.data()
              };
            });
    
            this.setState({threads:threads});
    
            if (this.state.loading) {
              this.setState({loading:false});
            }
          });
    
        /**
         * unsubscribe listener
         
        return () => unsubscribe();
      };
    
      if (loading: any) {
        return <Loading />;
      }
  render(){
    
    
      return (
        <View style={styles.container}>
          <FlatList
            data={this.state.threads}
            keyExtractor={item => item._id}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('AddRoom', { thread: item })}
              >
              </TouchableOpacity>
            )}
          />
        </View>
      );
    
  }
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#f5f5f5',
      flex: 1,
    },
    listTitle: {
      fontSize: 22,
    },
    listDescription: {
      fontSize: 16,
    },
  });

export default AddPost;
*/