import { StyleSheet } from 'react-native';
import colors from '../../../consts/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.WHITE
    },
    postImage: {
        borderWidth: 2
    },
    postView: {

    },
    postHeader: {
        flexDirection: "row"
    },
    postHeaderText: {
        marginTop: 16,
        fontWeight: "bold",
    },
    profileImage: {
        height: 40,
        width: 40,
        borderRadius: 50,
        margin: 8
    },
    postFooter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin:6
    },
    leftIcons: {
        flexDirection: 'row',
        width: 120,
        justifyContent: 'space-between',
    },
    addcomment:{
        marginTop:-10,
        marginLeft:5
    }
});

export default styles;