import React, { Component } from 'react';
import { View, Text, FlatList, Image, Dimensions,} from 'react-native';

import Ionicons from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";
import FontistoIcon from 'react-native-vector-icons/Fontisto';
import styles from './Posts.styles';
import { TextInput } from 'react-native-gesture-handler';
const dataArray = [
    { "id": "1", "name": "Davi", "imageUrl": "https://s3.amazonaws.com/arc-authors/washpost/e8d90017-3451-40a4-a668-901221acbb76.png" },
    { "id": "2", "name": "Shubham", "imageUrl": "https://scx2.b-cdn.net/gfx/news/hires/2019/2-nature.jpg" },
    { "id": "3", "name": "Anand", "imageUrl": "https://m.economictimes.com/thumb/msid-75989262,width-1200,height-900,resizemode-4,imgsize-198498/stock.jpg" },
    { "id": "4", "name": "Shadma", "imageUrl": "https://s3.amazonaws.com/arc-authors/washpost/e8d90017-3451-40a4-a668-901221acbb76.png" },
    { "id": "5", "name": "Manny", "imageUrl": "https://c0.wallpaperflare.com/preview/241/384/859/analysis-analytics-analyzing-annual.jpg" },
];

class Posts extends Component {
    constructor(props: any) {
        super(props);
    }

    render() {

        return (
            <View style={styles.container}>
                <FlatList
                    key={dataArray.id}
                    keyExtractor={item => item.id}
                    data={dataArray}
                    renderItem={({ item }) =>
                        <View style={styles.postView}>
                            <View style={styles.postHeader}>
                                <Image
                                    style={styles.profileImage}
                                    source={{ uri: item.imageUrl }}
                                />
                                <Text style={styles.postHeaderText}>{item.name}</Text>
                            </View>
                            <Image
                                style={styles.postImage}
                                source={{ uri: item.imageUrl }} height={300} width={Dimensions.get('screen').width}
                            />
                            <View style={styles.postFooter}>
                                <View style={styles.leftIcons}>
                                    <Feather
                                        name="heart" size={25} color={"#000"} />
                                    <FontistoIcon
                                        name="comment" size={25} color={"#000"} />
                                    <Ionicons
                                        name="paper-plane-outline" size={25} color={"#000"} />
                                </View>
                                <Feather name="bookmark" size={25} color={"#000"}/>
                            </View>
                            <View style={styles.addcomment}>
                                <TextInput
                                placeholder="Add a comment..."
                                ></TextInput>
                            </View>

                        </View>
                    }
                />
            </View>
        );
    }
}

export default Posts;