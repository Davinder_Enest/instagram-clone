import { StyleSheet } from 'react-native';
import colors from '../../../consts/colors';

const styles = StyleSheet.create({
    container: {
        height : 90,
        backgroundColor: colors.WHITE,
        flexDirection :'row',
        borderBottomWidth : 1,
        borderBottomColor : colors.GREY
    },
    images: {
        borderWidth: 2,
        height: 55,
        width : 55,
        borderRadius : 50,
        margin : 9
    },
    storyView:{
        justifyContent:"space-between",
        height:70,
    },
    storyText:{
        marginLeft : 19,
        marginTop: -5,
        fontSize : 12,
    }
});

export default styles;