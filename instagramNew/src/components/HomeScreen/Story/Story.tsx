import React, { Component } from 'react';
import { View, Text, FlatList, Image, ScrollView, StyleSheet, Dimensions } from 'react-native';
import styles from './Story.styles';

const dataArray = [
    { "id": "1", "name": "Your's", "imageUrl": "https://s3.amazonaws.com/arc-authors/washpost/e8d90017-3451-40a4-a668-901221acbb76.png" },
    { "id": "2", "name": "Sumit", "imageUrl": "https://c0.wallpaperflare.com/preview/241/384/859/analysis-analytics-analyzing-annual.jpg" },
    { "id": "3", "name": "Anuu", "imageUrl": "https://scx2.b-cdn.net/gfx/news/hires/2019/2-nature.jpg" },
    { "id": "4", "name": "Manny", "imageUrl": "https://m.economictimes.com/thumb/msid-75989262,width-1200,height-900,resizemode-4,imgsize-198498/stock.jpg" },
    { "id": "5", "name": "Ravina", "imageUrl": "https://s3.amazonaws.com/arc-authors/washpost/e8d90017-3451-40a4-a668-901221acbb76.png" },
    { "id": "6", "name": "Lovely", "imageUrl": "https://scx2.b-cdn.net/gfx/news/hires/2019/2-nature.jpg" },
    { "id": "7", "name": "David", "imageUrl": "https://s3.amazonaws.com/arc-authors/washpost/e8d90017-3451-40a4-a668-901221acbb76.png" }
];

class Story extends Component {
    constructor(props: any) {
        super(props);
    }

    render() {

        return (
            <View style={styles.container}>
                <FlatList
                    horizontal={true}
                    key={dataArray.id}
                    keyExtractor={item => item.id}
                    data={dataArray}
                    renderItem={({ item }) => 
                    <View style={styles.storyView}>
                        <Image style={styles.images}
                            source={{ uri: item.imageUrl }} 
                        />
                        <Text style={styles.storyText}>{item.name}</Text>
                    
                    </View>
                    }
                />
            </View>
        );
    }
}

export default Story;